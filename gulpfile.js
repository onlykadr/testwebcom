"use strict";

const gulp = require('gulp'),
      pug = require('gulp-pug'),
      stylus = require('gulp-stylus'),
      resolver = require('stylus').resolver,
      sourcemaps = require('gulp-sourcemaps'),
      concat = require('gulp-concat'),
      debug = require('gulp-debug'),
      plumber = require('gulp-plumber'),
      prefix = require('gulp-autoprefixer'),
      imagemin = require('gulp-imagemin'),
      browserSync = require('browser-sync').create();

const gulpif = require('gulp-if'),
      rename = require('gulp-rename'),
      cssmin = require('gulp-clean-css'),
      rimraf = require('rimraf'),
      newer = require('gulp-newer'),
      notify = require('gulp-notify'),
      uglify = require('gulp-uglify'),
      spritesmith = require('gulp.spritesmith')

let paths = {
      blocks: 'blocks/',
      devDir: 'app/',
      tmp: './tmp/style/',
      outputDir: 'build/'
    };

let isProd = false,
    isDev = false;

gulp.task('set:dev:env', function(cb) {
    process.env.NODE_ENV = 'dev';
    isDev = true;
    cb();
});

gulp.task('set:prod:env', function (cb) {
    process.env.NODE_ENV = 'prod';
    isProd = true;
    cb();
});

/*********************************
    Developer tasks
*********************************/

//pug compile
gulp.task('pug', function() {
  return gulp.src(paths.blocks + '*.pug')
    .pipe(plumber({
      errorHandler: notify.onError(function(err) {
        return{
          title: 'PUG',
          message: err.message
        };
      })
    }))
    .pipe(rename('index.pug'))
    .pipe(pug({pretty: true}))
    .pipe(gulpif(isDev, gulp.dest(paths.devDir)))
    .pipe(gulpif(isProd, gulp.dest(paths.outputDir)))
});

// stylus compile
gulp.task('styles', function(){
  return gulp.src(paths.blocks + '/*.styl')
    .pipe(plumber({
      errorHandler: notify.onError(function(err) {
        return{
          title: 'Styles',
          message: err.message
        };
      })
    }))
    .pipe(gulpif(isDev, sourcemaps.init()))
    .pipe(stylus({
      'include css': true,
      import: process.cwd() + '/tmp/style/sprite',
      define: {
        url: resolver()
      }
    }))
    .pipe(prefix())
    .pipe(gulpif(isDev, sourcemaps.write('.')))
    .pipe(gulpif(isDev, gulp.dest(paths.devDir)))
    .pipe(gulpif(isProd, cssmin()))
    .pipe(gulpif(isProd, gulp.dest(paths.outputDir)))
});

//js lib compile
gulp.task('scripts:lib', function() {
  return gulp.src(paths.blocks + 'assets/js/lib/*.js')
    .pipe(plumber({
      errorHandler: notify.onError(function(err) {
        return{
          title: 'Scripts',
          message: err.message
        };
      })
    }))
    .pipe(concat('lib.js'))
    .pipe(gulpif(isDev, gulp.dest(paths.devDir + 'assets/js')))
    .pipe(gulpif(isProd, uglify()))
    .pipe(gulpif(isProd, gulp.dest(paths.outputDir + 'assets/js')))
});

//js compile
gulp.task('scripts', function() {
  return gulp.src(paths.blocks + 'assets/js/*.js')
    .pipe(plumber({
      errorHandler: notify.onError(function(err) {
        return{
          title: 'Scripts',
          message: err.message
        };
      })
    }))
    .pipe(concat('script.js'))
    .pipe(gulpif(isDev, gulp.dest(paths.devDir + 'assets/js')))
    .pipe(gulpif(isProd, gulp.dest(paths.outputDir + 'assets/js')))
});


//create sprite
gulp.task('sprite', function(cb) {
  var spriteData = 
    gulp.src(paths.blocks + 'b-*/**/*.png')
    .pipe(spritesmith({
      imgName: 'sprite.png',
      cssName: 'sprite.styl',
      cssFormat: 'stylus',
      algorithm: 'binary-tree',
      cssTemplate: 'stylus.template.mustache',
      cssVarMap: function(sprite) {
        sprite.name = 's-' + sprite.name
      }
    }));

  spriteData.img.pipe(gulpif(isDev, gulp.dest(paths.devDir + 'assets/img/sprite')));
  spriteData.img.pipe(gulpif(isProd, gulp.dest(paths.outputDir + 'assets/img/sprite')));
  spriteData.css.pipe(gulp.dest(paths.tmp));
  cb();
});

//assets
gulp.task('assets', function(){
  return gulp.src(
    paths.blocks + 'assets/img/favicon/**',
    {
      base: paths.blocks
    }
  )
    .pipe(newer(paths.devDir + 'assets/img'))
    .pipe(gulpif(isDev, gulp.dest(paths.devDir)))
    .pipe(gulpif(isProd, gulp.dest(paths.outputDir)))
});

//block:assets
gulp.task('block:assets', function(){
  return gulp.src(paths.blocks + 'b-*/**/*.{jpg,jpeg}')
    .pipe(gulpif(isDev, gulp.dest(paths.devDir)))
    .pipe(gulpif(isProd, gulp.dest(paths.outputDir)))
});

//clean
gulp.task('clean', function(cb) {
  rimraf(paths.devDir, cb);
  rimraf(paths.outputDir, cb);
});

//watch
gulp.task('watch', function() {
  gulp.watch(paths.blocks + '**/*.pug', gulp.series('pug'));
  gulp.watch(paths.blocks + '**/*.styl', gulp.series('styles'));
  gulp.watch(paths.blocks + 'b-*/**/*.{jpg,}', gulp.series('block:assets'));
  gulp.watch(paths.blocks + 'b-*/**/*.{png}', gulp.series('sprite'));
  gulp.watch(paths.blocks + 'assets/**/*.*', gulp.series('assets'));
  gulp.watch(paths.blocks + '**/*.js', gulp.series('scripts'));
});

//serve 
gulp.task('serve', function(){
  browserSync.init({
    server: paths.devDir
  });
  browserSync.watch(paths.devDir + '**/*.*')
  .on('change', browserSync.reload);
});

//build
gulp.task('build', gulp.series('sprite', 'block:assets', 'assets', 'styles', 'scripts:lib', 'scripts', 'pug'));

//dev
gulp.task('dev', gulp.series('set:dev:env', 'clean', 'build', gulp.parallel('watch', 'serve')));

/*********************************
    Production tasks
*********************************/

// prod
gulp.task('prod', gulp.series('set:prod:env', 'clean', 'build'))

