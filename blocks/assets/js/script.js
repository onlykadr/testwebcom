'use strict';

//slider
var swiperH = new Swiper('.swiper-container-h', {
  pagination: '.swiper-pagination',
  paginationClickable: true,
  spaceBetween: 50,
  nextButton: '.swiper-button-next',
  prevButton: '.swiper-button-prev',
});
var swiperV = new Swiper('.swiper-container-v', {
  pagination: '.swiper-pagination-v',
  paginationClickable: true,
  direction: 'vertical',
  spaceBetween: 50,
  mousewheelControl: true,
  keyboardControl: true,
  freeMode: true,
  slidesPerView: 'auto',
  hashnav: true,
  hashnavWatchState: true
});

let scrollbar = document.querySelector('.swiper-pagination-v'),
    btnDown = document.querySelector('.header__btn-down'),
    btnUp = document.querySelector('.footer__btn-up'),
    btnOpenOrder = document.querySelector('header .js-open-popup-order'),
    nav = document.querySelector('.nav'),
    order = document.querySelector('.order'),
    openMenu = document.querySelector('.js-open-menu'),
    closeMenu = document.querySelector('.js-close-menu'),
    navItems = document.querySelectorAll('.nav__item'),
    secHash = document.querySelectorAll('[data-hash]'),
    header = document.querySelector('.header'),
    headerLine = document.querySelector('.header-line'),
    addressBtn = document.querySelector('.address__btn'),
    addressItem = document.querySelector('.address__item');

//first scan = #general added active
  navItems[0].classList.add('nav__item--active');

  window.addEventListener('wheel', sectionActive);
  nav.addEventListener('click', delay);
  scrollbar.addEventListener('click', delay);
  btnDown.addEventListener('click', delay);
  btnUp.addEventListener('click', delay);

//need delay after scroll (sectionActive not working)
  function delay() {
    setTimeout(function() {
      sectionActive()
    }, 50)
  };

//open popup
  btnOpenOrder.addEventListener('click', function() {
    order.style.display = 'flex';
  });

  order.addEventListener('click', function(event){
    let target = event.target;
    if (target.classList.contains('js-close-popup')) {
      order.style.display = '';
      return;
    }
  });

//delegation 'click' open/close menu or popup
  headerLine.addEventListener('click', function(event) {
    let target = event.target;
    while (target != this) {
      if (target.classList.contains('js-open-menu')) {
        target.style.display = 'none';
        closeMenu.style.display = 'flex';
        nav.style.display = 'flex';
        return;
      }
      if (target.classList.contains('js-close-menu')) {
        target.style.display = '';
        openMenu.style.display = 'block';
        nav.style.display = '';
        return;
      }
      if (target.classList.contains('js-open-popup-order')) {
        order.style.display = 'flex';
        return;
      }
      target = target.parentNode;
    }
  });

//menu item add class active when active corresponding section
  function sectionActive() {
    for(let i = 0; i < secHash.length; i++) {
      if(secHash[i].classList.contains('swiper-slide-active')){
        let name = secHash[i].getAttribute('data-hash');
        resizeMenu(name);
        for(let i = 0; i < navItems.length; i++) {
          if (navItems[i].getAttribute('name') == name){
            navItems[i].classList.add('nav__item--active');
          }
          else {
            navItems[i].classList.remove('nav__item--active');
          }
        }
      }
    }
  };

  function resizeMenu(name){
    if (name == 'general'){
      headerLine.style.height = '120px';
      addressBtn.style.display = '';
      addressItem.style.display = '';
    }
    else {
      headerLine.style.height = '80px';
      addressBtn.style.display = 'block';
      addressItem.style.display = 'none';
    }
  };
